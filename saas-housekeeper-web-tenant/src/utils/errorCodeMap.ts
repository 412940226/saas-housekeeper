import { i18n, t } from "@/i18n";
import { computed, ComputedRef } from 'vue'

const errorCodes = [
    110001, // 没有权限
    110002, // 业务异常
    110003, // 未知异常
    110004, // 参数非法
    110005, // 用户名或者密码错误
    110006, // token过期
    110007, // token错误
    110008, // 参数为空
    110009, // json异常
    110010, // 空指针异常
    110011, // 路由失败

    220000, // 服务名重复
    220001, // 服务不存在
    220007, // 服务规格已存在
    220012, // 服务规格不存在
    220008, // 该skuId没有对应的数据
    220009, // 服务选集重复
    220010, // 主键不能为空
    220011, // 数据逻辑非法

    330003, // 没有此项服务
    330001, // 用户id为空
    330002, // 状态更改异常
    330004, // 消息接收失败

    440000, // 缺失租户标识
    440002, // 任务不存在
    440003, // 订单任务已存在
    440004, // 用户电话必填
    440009, // 参数必填
    440008, // 消息接收失败
    440010, // 取消操作，任务的状态必须是已接单
    440011, // 任务完成操作，任务的状态必须是已接单
    440012, // 抢单操作，任务的状态必须是待抢单
    440013, // 任务操作非法
    440014, // 订单任务已取消

    550001, // 没有权限
    550002, // 业务异常
    550003, // 未知异常
    550004, // 参数非法
    550005, // 用户名或者密码错误
    550006, // token过期
    550007, // token错误
    550008, // 参数为空
    550009, // 用户名重复
    550010, // 不能删除当前用户
    550011, // 用户不存在
    660001, // 名字/域名/企业信用码重复
    660002, // 租户数据源为空
    660003, // 不能重复更改状态
] as const

export type ErrorCodeTuple = typeof errorCodes;
export type ErrorCode = ErrorCodeTuple[number]

export const ErrorCodeMap = errorCodes.reduce((map, item) => {
    map[item] = computed(() => t(`responseMsg.codes.${item}`))
    return map
}, {} as Record<ErrorCode, ComputedRef<string>>)

/**
 * Check if all error message in locale messages
 * 检查错误码是否设置了 locale，防止漏写词条
 */
function checkCodesAndLocaleMessageSetting() {
    const codesMap = i18n.global.getLocaleMessage(i18n.global.locale.value).responseMsg.codes
    const lackCodes: ErrorCode[] = [];
    errorCodes.forEach(errorCode => {
        if (codesMap[errorCode] === undefined || codesMap[errorCode] === null) {
            lackCodes.push(errorCode);
        }
    })
    if (lackCodes.length) {
        const errorMsg = `In locale messages 'responseMsg.codes', error ${lackCodes.length === 1 ? 'code' : 'codes'} [${lackCodes}] missing.`
        console.error(errorMsg);
    }
}
checkCodesAndLocaleMessageSetting()
