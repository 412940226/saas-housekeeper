import HttpRequest from './axios';

const baseLogUrl = '/api-log/';

// 2.根据process.env.NODE_ENV区分
// 开发环境: development
// 生成环境: production
// 测试环境: test

// if (process.env.NODE_ENV === 'development') {
//   BASE_URL = '/api'
// } else if (process.env.NODE_ENV === 'production') {
//   BASE_URL = '/api'
// } else {
//   BASE_URL = '/api'
// }
const BASE_URL = process.env.VUE_APP_BASE_URL;

const axios = new HttpRequest(BASE_URL);
export { axios, baseLogUrl };
