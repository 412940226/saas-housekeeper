/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.model;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 身份集合
 *
 * @author swx1102895
 * @since 2022-10-12
 */
@Data
@Component
@Configuration
@ConfigurationProperties(prefix = "identity.list")
public class ListAdminContext {

    private List<AdminContext> adminContext;

}
