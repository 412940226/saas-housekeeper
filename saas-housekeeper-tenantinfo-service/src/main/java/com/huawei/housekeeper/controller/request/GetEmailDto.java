package com.huawei.housekeeper.controller.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 功能描述
 *
 * @since 2022-10-20
 */
@Data
public class GetEmailDto {
    @ApiModelProperty(value = "id", required = true)
    private Long id;
}