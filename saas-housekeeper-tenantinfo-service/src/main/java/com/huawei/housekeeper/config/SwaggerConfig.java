/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.config;

import com.huawei.housekeeper.constants.BaseConstant;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;

/**
 * API文档
 *
 * @author lWX1128557
 * @since 2022-03-03
 */
@Configuration
@EnableSwagger2
@RefreshScope
public class SwaggerConfig {
    @Value("${swagger.profiles}")
    private String swaggerProfiles;

    @Value("${swagger.basePackage}")
    private String basePackage;

    /**
     * swagger文档主题配置
     *
     * @param environment 环境
     * @return Docket 摘要
     */
    @Bean
    public Docket docket(Environment environment) {
        // 截取swagger环境配置
        String[] profileStr = swaggerProfiles.split(BaseConstant.Symbol.COMMA);
        // 配置
        Profiles profiles = Profiles.of(profileStr);
        boolean isShow = environment.acceptsProfiles(profiles);
        return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()) // 文档信息
            .groupName("租户模块API") // 文档组标题
            .enable(isShow) // 是否启用swagger摘要
            .select() // 扫描配置接口
            .apis(RequestHandlerSelectors.basePackage(basePackage)) // 指定扫描的api包
            .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().version("1.0") // 文档版本
            .description("家政服务saas的tenantinfo模块") // 文档描述
            .title("租户模块API") // 文档标题
            .build();
    }
}
