/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 查询用户对象
 *
 * @author wwx1136431
 * @since 2022/3/18 15:18
 */
@Setter
@Getter
@ApiModel("租户查询用户列表")
public class GetTenantUserInfoDto {
    @ApiModelProperty(value = "用户id", required = true)
    private String userId;

    @ApiModelProperty(value = "用户名", required = true)
    private String userName;

    @ApiModelProperty(value = "手机号", required = true)
    private String phoneNo;

    @ApiModelProperty(value = "用户角色", required = true)
    private Integer userRole;
}
