/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.request;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 查询用户对象
 *
 * @author l84165417
 * @since 2022/1/26 15:18
 */
@Setter
@Getter
@ApiModel("查询用户对象")
public class GetUserDto {
    @NotBlank(message = "用户名必填")
    @Length(min = 5, max = 35, message = "用户名长度范围: 5-35")
    @ApiModelProperty(value = "用户名", required = true)
    private String userName;

    @ApiModelProperty(value = "用户ID", required = true)
    private String userId;
}
