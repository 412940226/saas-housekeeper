/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 查询用户响应vo
 *
 * @author l84165417
 * @since 2022/1/26 15:18
 */
@Getter
@Setter
@ApiModel("查询用户响应vo")
public class GetUserVo {
    @ApiModelProperty("用户id")
    private String usrId;

    @ApiModelProperty("用户名")
    private String userName;

    @ApiModelProperty("用户角色")
    private Integer userRole;
}
