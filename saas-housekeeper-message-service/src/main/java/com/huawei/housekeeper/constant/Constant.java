/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.constant;

/**
 * 常量
 *
 * @since 2022-02-23
 */
public interface Constant {

    /**
     * 消息状态
     */
    interface MessageStatus {
        /**
         * 未读
         */
        int UN_READ = 0;

        /**
         * 已读
         */
        int READ_ED = 1;

        /**
         * 已删除
         */
        int DEL = 2;

    }
}
