/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.saashousekeeper.dbpool.druid;

import com.huawei.saashousekeeper.constants.DbPoolEnum;
import com.huawei.saashousekeeper.customedprocessor.PoolRefreshProcessor;
import com.huawei.saashousekeeper.dbpool.JdbcPool;

import com.alibaba.druid.pool.DruidDataSource;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.log4j.Log4j2;

import java.io.Serializable;

/**
 * Druid参数配置
 *
 * @author lWX1156935
 * @since 2022/4/22
 */
@Data()
@EqualsAndHashCode(callSuper = false)
@Log4j2
@JsonIgnoreProperties(ignoreUnknown = true)
public class DruidPool extends DruidDataSource implements JdbcPool, Serializable {
    private static final long serialVersionUID = 1L;

    @Override
    public String getPoolName() {
        return DbPoolEnum.POOL_DRUID.getName();
    }

    @Override
    public boolean refresh(JdbcPool jdbcPool, PoolRefreshProcessor processor) {
        return processor != null && processor.check(this, jdbcPool);
    }
}