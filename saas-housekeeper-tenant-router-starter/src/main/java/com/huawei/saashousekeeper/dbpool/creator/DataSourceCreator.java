/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.saashousekeeper.dbpool.creator;

import com.huawei.saashousekeeper.dbpool.PoolStrategy;
import com.huawei.saashousekeeper.properties.DataSourceProperty;

import javax.sql.DataSource;

/**
 * 创建数据源
 *
 * @author lWX1156935
 * @since 2022/4/22
 */
public interface DataSourceCreator extends PoolStrategy {
    /**
     * 通过属性创建数据源
     *
     * @param dataSourceProperty 数据源属性
     * @return 被创建的数据源
     */
    DataSource createDataSource(DataSourceProperty dataSourceProperty);

    /**
     * 关闭连接池
     *
     * @param dataSource 数据源
     * @return 关闭结果
     */
    boolean close(DataSource dataSource);
}
