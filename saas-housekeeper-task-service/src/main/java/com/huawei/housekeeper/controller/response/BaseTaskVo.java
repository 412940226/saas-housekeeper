/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 功能描述
 *
 * @since 2022-03-10
 */
@Getter
@Setter
public class BaseTaskVo {
    @ApiModelProperty("任务Id")
    private Long id;

    @ApiModelProperty("顾客id;用户中心上下文")
    private String customerId;

    @ApiModelProperty("订单id;订单中心上下文")
    private Long orderId;

    @ApiModelProperty("服务时间")
    private Date appointmentTime;

    @ApiModelProperty("地址")
    private String address;

    @ApiModelProperty("服务名称;订单中心推送")
    private String serviceName;

    @ApiModelProperty("服务细节;订单中心推送")
    private String serviceDetail;

    @ApiModelProperty("佣金,可通过支付金额得出")
    private BigDecimal salary;

    @ApiModelProperty("雇佣名称")
    private String employeeName;

    @ApiModelProperty("雇佣id")
    private String employeeId;

    @ApiModelProperty("订单数量")
    private Integer amount;

    @ApiModelProperty("任务状态")
    private String taskStatus;

    @ApiModelProperty("订单备注")
    private String remark;

    @ApiModelProperty("创建时间")
    private Date createdTime;

    @ApiModelProperty("更新时间")
    private Date updatedTime;
}