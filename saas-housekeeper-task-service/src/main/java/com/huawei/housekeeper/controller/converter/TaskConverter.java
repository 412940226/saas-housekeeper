/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.converter;

import com.huawei.housekeeper.controller.request.DoTaskDto;
import com.huawei.housekeeper.controller.response.MyTaskDetailVo;
import com.huawei.housekeeper.controller.response.MyTaskListVo;
import com.huawei.housekeeper.controller.response.TaskDetailVo;
import com.huawei.housekeeper.controller.response.TaskPageListVo;
import com.huawei.housekeeper.dao.entity.Task;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * mapstruct转换器
 *
 * @author jwx1116205
 * @since 2022-03-02
 */
@Mapper(componentModel = "spring")
public interface TaskConverter {
    /**
     * 对象映射转换实例
     */
    TaskConverter INSTANCE = Mappers.getMapper(TaskConverter.class);

    /**
     * 数据转换成前端显示
     *
     * @param tasks
     * @return TaskVo
     */
    List<TaskPageListVo> toTaskPageListVo(List<Task> tasks);

    /**
     * 任务详情，返回任务详情，除了用户名称和电话
     *
     * @param task
     * @return {@link TaskDetailVo}
     */
    TaskDetailVo toTaskDetailVo(Task task);

    /**
     * 任务详情，返回任务详情，除了用户名称和电话
     *
     * @param task
     * @return {@link TaskDetailVo}
     */
    MyTaskDetailVo toMyTaskDetailVo(Task task);

    /**
     * 查询个人已接任务
     *
     * @param taskList
     * @return {@link MyTaskListVo}
     */
    List<MyTaskListVo> toMyTaskDetailListVo(List<Task> taskList);

    /**
     * 抢单对象转成数据库实例
     *
     * @param taskDto
     * @return Task
     */
    Task createTaskDtoToTask(DoTaskDto taskDto);
}