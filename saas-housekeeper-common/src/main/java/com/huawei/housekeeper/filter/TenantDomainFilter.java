/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.filter;

import com.huawei.saashousekeeper.properties.ResourcesExcluderProperties;
import com.huawei.saashousekeeper.config.binding.SchemaBindingStrategy;
import com.huawei.saashousekeeper.constants.Constants;
import com.huawei.saashousekeeper.context.TenantContext;
import com.huawei.saashousekeeper.exception.RoutingException;
import lombok.extern.log4j.Log4j2;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.Ordered;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

/**
 * 租户标识过滤器
 *
 * @author lWX1128557
 * @since 2022-04-01
 */
@Log4j2
public class TenantDomainFilter extends OncePerRequestFilter implements Ordered {
    private final AntPathMatcher pathMatcher = new AntPathMatcher();

    @Autowired
    private SchemaBindingStrategy schemaAdapter;

    @Autowired
    @Qualifier("handlerExceptionResolver")
    private HandlerExceptionResolver resolver;

    @Autowired
    private ResourcesExcluderProperties ResourcesExcluderProperties;

    @Override
    public int getOrder() {
        return HIGHEST_PRECEDENCE;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String tenantDomain = request.getHeader(Constants.TENANT_DOMAIN);
        TenantContext.setDomain(tenantDomain, true);
        // MDC埋点
        MDC.put(Constants.TENANT_ID, tenantDomain);

        // 过滤资源
        String[] excludedUris = ResourcesExcluderProperties.getExcludedUris();
        for (String uris : excludedUris) {
            if (pathMatcher.match(uris, request.getRequestURI())) {
                log.info("-->Tenant ID filter, filtering out url={}: " + request.getRequestURI());
                filterChain.doFilter(request, response);
                TenantContext.remove();
                MDC.clear();
                return;
            }
        }
        try {
            if (Objects.isNull(schemaAdapter.getSchema(tenantDomain))) {
                throw new RoutingException("Routing failure,The corresponding database could not be found");
            }
            filterChain.doFilter(request, response);
        } catch (Exception e) {
            resolver.resolveException(request, response, null, e);
        } finally {
            TenantContext.remove();
            MDC.clear();
        }
    }
}
