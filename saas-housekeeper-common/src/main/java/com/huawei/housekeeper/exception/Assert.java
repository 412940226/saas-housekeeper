/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.exception;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.function.BiFunction;
import java.util.function.Supplier;

/**
 * 通用的错误码断言类
 *
 * @author q00197955
 * @version [版本号, 2019-05-28]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public abstract class Assert {
    /**
     * 非空断言
     *
     * @param param 判断的对象
     * @param message 异常消息
     * @param <T> 泛型
     * @return 结果
     * @throws ErrorCodeException 异常
     */
    public final static <T> T notNull(T param, String message) throws ParameterNotValidException {
        if (null == param) {
            throw new ParameterNotValidException(message);
        }

        return param;
    }

    /**
     * 非空断言
     *
     * @param param 判断的对象
     * @param messageSupplier 异常消息
     * @param <T> 泛型
     * @return 结果
     * @throws ErrorCodeException 异常
     */
    public final static <T> T notNull(T param, Supplier<String> messageSupplier) throws ParameterNotValidException {
        if (null == param) {
            throw new ParameterNotValidException(messageSupplier.get());
        }

        return param;
    }

    /**
     * 非空断言
     *
     * @param param 判断的对象
     * @param errorCode 错误码
     * @param message 错误描述
     * @param <T> 泛型
     * @return 结果
     * @throws ErrorCodeException 异常
     */
    public final static <T> T notNull(T param, int errorCode, String message) throws ErrorCodeException {
        if (null == param) {
            throw new ErrorCodeException(errorCode, message);
        }

        return param;
    }

    /**
     * 非空断言
     *
     * @param param 判断的对象
     * @param errorCode 错误码
     * @param messageSupplier 错误描述
     * @param <T> 泛型
     * @return 结果
     * @throws ErrorCodeException 异常
     */
    public final static <T> T notNull(T param, int errorCode, Supplier<String> messageSupplier)
        throws ErrorCodeException {
        if (null == param) {
            throw new ErrorCodeException(errorCode, messageSupplier.get());
        }

        return param;
    }

    /**
     * 非空断言
     *
     * @param param 判断的对象
     * @param messageSupplier 错误描述
     * @return 结果
     * @throws ErrorCodeException 异常
     */
    public final static String notBlank(String param, Supplier<String> messageSupplier)
        throws ParameterNotValidException {
        if (StringUtils.isBlank(param)) {
            throw new ParameterNotValidException(messageSupplier.get());
        }

        return param;
    }

    /**
     * 非空断言
     *
     * @param param 判断的对象
     * @param message 错误描述
     * @return 结果
     * @throws ErrorCodeException 异常
     */
    public final static String notBlank(String param, String message) throws ParameterNotValidException {
        if (StringUtils.isBlank(param)) {
            throw new ParameterNotValidException(message);
        }

        return param;
    }

    /**
     * 非空串断言
     *
     * @param param 参数
     * @param errorCode 错误码
     * @param message 错误描述
     * @return 返回非空参数
     * @throws ErrorCodeException 异常
     */
    public final static String notBlank(String param, int errorCode, String message) throws ErrorCodeException {
        if (StringUtils.isBlank(param)) {
            throw new ErrorCodeException(errorCode, message);
        }

        return param;
    }

    /**
     * 非空串断言
     *
     * @param param 参数
     * @param errorCode 错误码
     * @param messageSupplier 错误描述Supplier
     * @return 非空参数
     * @throws ErrorCodeException
     */
    public final static String notBlank(String param, int errorCode, Supplier<String> messageSupplier)
        throws ErrorCodeException {
        if (StringUtils.isBlank(param)) {
            throw new ErrorCodeException(errorCode, messageSupplier.get());
        }

        return param;
    }

    /**
     * 非空断言
     *
     * @param param 参数
     * @param message 错误描述
     * @param <T> 参数类型
     * @return 返回非空参数
     * @throws ErrorCodeException 异常
     */
    public final static <T> T[] notEmpty(T[] param, String message) throws ParameterNotValidException {
        if (ArrayUtils.isEmpty(param)) {
            throw new ParameterNotValidException(message);
        }

        return param;
    }

    /**
     * 非空
     *
     * @param param 参数
     * @param messageSupplier 错误消息
     * @return 类型
     * @throws ErrorCodeException 异常
     */
    public final static <T> T[] notEmpty(T[] param, Supplier<String> messageSupplier)
        throws ParameterNotValidException {
        if (ArrayUtils.isEmpty(param)) {
            throw new ParameterNotValidException(messageSupplier.get());
        }
        return param;
    }

    /**
     * 非空断言
     *
     * @param param 参数
     * @param errorCode 错误码
     * @param message 错误描述
     * @param <T> 参数类型
     * @return 返回非空参数
     * @throws ErrorCodeException 异常
     */
    public final static <T> T[] notEmpty(T[] param, int errorCode, String message) throws ErrorCodeException {
        if (ArrayUtils.isEmpty(param)) {
            throw new ErrorCodeException(errorCode, message);
        }

        return param;
    }

    /**
     * 非空断言
     *
     * @param param 参数
     * @param errorCode 错误码
     * @param messageSupplier 错误描述
     * @param <T> 参数类型
     * @return 返回非空参数
     * @throws ErrorCodeException 异常
     */
    public final static <T> T[] notEmpty(T[] param, int errorCode, Supplier<String> messageSupplier)
        throws ErrorCodeException {
        if (ArrayUtils.isEmpty(param)) {
            throw new ErrorCodeException(errorCode, messageSupplier.get());
        }

        return param;
    }

    /**
     * 非空断言
     *
     * @param param 参数
     * @param message 错误描述
     * @param <T> 参数类型
     * @return 返回非空参数
     * @throws ErrorCodeException 异常
     */
    public final static <T extends Collection<?>> T notEmpty(T param, String message)
        throws ParameterNotValidException {
        if (CollectionUtils.isEmpty(param)) {
            throw new ParameterNotValidException(message);
        }

        return param;
    }

    /**
     * 非空断言
     *
     * @param param 参数
     * @param errorCode 错误码
     * @param message 错误描述
     * @return 参数
     * @throws ErrorCodeException 异常
     */
    public final static String notEmpty(String param, int errorCode, String message) throws ErrorCodeException {
        if (StringUtils.isEmpty(param)) {
            throw new ErrorCodeException(errorCode, message);
        }
        return param;
    }

    /**
     * 非空断言
     *
     * @param param 参数
     * @param message 错误描述
     * @return 参数
     * @throws ErrorCodeException
     */
    public final static String notEmpty(String param, String message) throws ParameterNotValidException {
        if (StringUtils.isEmpty(param)) {
            throw new ParameterNotValidException(message);
        }
        return param;
    }

    /**
     * 非空断言
     *
     * @param param 参数
     * @param messageSupplier 错误描述Supplier
     * @param <T> 参数类型
     * @return 参数
     * @throws ErrorCodeException
     */
    public final static <T extends Collection<?>> T notEmpty(T param, Supplier<String> messageSupplier)
        throws ParameterNotValidException {
        if (CollectionUtils.isEmpty(param)) {
            throw new ParameterNotValidException(messageSupplier.get());
        }

        return param;
    }

    /**
     * 非空断言
     *
     * @param param 参数
     * @param errorCode 错误码
     * @param message 错误描述
     * @param <T> 参数类型
     * @return 参数
     * @throws ErrorCodeException 异常
     */
    public final static <T extends Collection<?>> T notEmpty(T param, int errorCode, String message)
        throws ErrorCodeException {
        if (CollectionUtils.isEmpty(param)) {
            throw new ErrorCodeException(errorCode, message);
        }

        return param;
    }

    /**
     * 非空断言
     *
     * @param param 参数
     * @param errorCode 错误码
     * @param messageSupplier 错误描述
     * @param <T> 参数类型
     * @return 参数
     * @throws ErrorCodeException 异常
     */
    public final static <T extends Collection<?>> T notEmpty(T param, int errorCode, Supplier<String> messageSupplier)
        throws ErrorCodeException {
        if (CollectionUtils.isEmpty(param)) {
            throw new ErrorCodeException(errorCode, messageSupplier.get());
        }

        return param;
    }

    /**
     * 真断言
     *
     * @param param 参数
     * @param message 错误描述
     * @throws ErrorCodeException 异常
     */
    public final static void isTrue(boolean param, String message) throws ParameterNotValidException {
        if (!param) {
            throw new ParameterNotValidException(message);
        }
    }

    /**
     * 真断言
     *
     * @param param 参数
     * @param messageSupplier 错误描述
     * @throws ErrorCodeException 异常
     */
    public final static void isTrue(boolean param, Supplier<String> messageSupplier) throws ParameterNotValidException {
        if (!param) {
            throw new ParameterNotValidException(messageSupplier.get());
        }
    }

    /**
     * 真断言
     *
     * @param param 参数
     * @param errorCode 错误码
     * @param message 错误描述
     * @throws ErrorCodeException 异常
     */
    public final static void isTrue(boolean param, int errorCode, String message) throws ErrorCodeException {
        if (!param) {
            throw new ErrorCodeException(errorCode, message);
        }
    }

    /**
     * 真断言
     *
     * @param param 参数
     * @param errorCode 错误码
     * @param messageSupplier 错误描述
     * @throws ErrorCodeException 异常
     */
    public final static void isTrue(boolean param, int errorCode, Supplier<String> messageSupplier)
        throws ErrorCodeException {
        if (!param) {
            throw new ErrorCodeException(errorCode, messageSupplier.get());
        }
    }

    /**
     * 假断言
     *
     * @param param 参数
     * @param errorCode 错误码
     * @param message 错误描述
     * @throws ErrorCodeException 异常
     */
    public final static void isFalse(boolean param, int errorCode, String message) throws ErrorCodeException {
        if (param) {
            throw new ErrorCodeException(errorCode, message);
        }
    }

    /**
     * 假断言
     *
     * @param param 参数
     * @param errorCode 错误码
     * @param messageSupplier 错误描述
     * @throws ErrorCodeException 异常
     */
    public final static void isFalse(boolean param, int errorCode, Supplier<String> messageSupplier)
        throws ErrorCodeException {
        if (param) {
            throw new ErrorCodeException(errorCode, messageSupplier.get());
        }
    }

    /**
     * 非空断言
     *
     * @param param 参数
     * @param errorCode 错误码
     * @param message 错误描述
     * @param function function
     * @param <T> 参数
     * @param <E> 异常类型
     * @return 是否为空
     * @throws E 异常类型
     */
    public final static <T, E extends ErrorCodeException> T notNull(T param, int errorCode, String message,
        BiFunction<Integer, String, E> function) throws E {
        if (null == param) {
            throw function.apply(errorCode, message);
        }
        return param;
    }

    /**
     * 非空串断言
     *
     * @param param 参数
     * @param errorCode 错误码
     * @param message 错误描述
     * @param function function
     * @param <E> 异常类型
     * @return 结果
     * @throws E 异常类型
     */
    public final static <E extends ErrorCodeException> String notBlank(String param, int errorCode, String message,
        BiFunction<Integer, String, E> function) throws E {
        if (StringUtils.isBlank(param)) {
            throw function.apply(errorCode, message);
        }
        return param;
    }

    /**
     * 非空串断言
     *
     * @param param 参数
     * @param errorCode 错误码
     * @param message 错误描述
     * @param function 方法
     * @param <T> 参数类型
     * @param <E> 异常类型
     * @return 结果
     * @throws E 异常
     */
    public final static <T, E extends ErrorCodeException> T[] notEmpty(T[] param, int errorCode, String message,
        BiFunction<Integer, String, E> function) throws E {
        if (ArrayUtils.isEmpty(param)) {
            throw function.apply(errorCode, message);
        }

        return param;
    }

    /**
     * 非空断言
     *
     * @param param 参数
     * @param errorCode 错误码
     * @param message 错误描述
     * @param function 方法
     * @param <T> 参数类型
     * @param <E> 异常类型
     * @return 结果
     * @throws E 异常
     */
    public final static <T extends Collection<?>, E extends ErrorCodeException> T notEmpty(T param, int errorCode,
        String message, BiFunction<Integer, String, E> function) throws E {
        if (CollectionUtils.isEmpty(param)) {
            throw function.apply(errorCode, message);
        }

        return param;
    }

    /**
     * 真断言
     *
     * @param param 参数
     * @param errorCode 错误码
     * @param message 错误描述
     * @param function 方法
     * @param <E> 异常类型
     * @throws E 异常
     */
    public final static <E extends ErrorCodeException> void isTrue(boolean param, int errorCode, String message,
        BiFunction<Integer, String, E> function) throws E {
        if (!param) {
            throw function.apply(errorCode, message);
        }
    }

    /**
     * 假断言
     *
     * @param param 参数
     * @param errorCode 错误码
     * @param message 错误描述
     * @param function 方法
     * @param <E> 异常类型
     * @throws E 异常
     */
    public final static <E extends ErrorCodeException> void isFalse(boolean param, int errorCode, String message,
        BiFunction<Integer, String, E> function) throws E {
        if (param) {
            throw function.apply(errorCode, message);
        }
    }
}