/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.config;

import com.huawei.housekeeper.filter.TenantDomainFilter;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AutoConfiguration {

    /**
     * 租户标识过滤器
     *
     * @return TenantDomainFilter
     */
    @Bean
    public TenantDomainFilter tenantDomainFilter() {
        return new TenantDomainFilter();
    }

    /**
     * 注册过滤器
     *
     * @return 过滤器
     */
    @Bean(value = "routingFilter")
    public FilterRegistrationBean filterRegistrationBean() {
        FilterRegistrationBean<TenantDomainFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(tenantDomainFilter());
        return registrationBean;
    }

}
