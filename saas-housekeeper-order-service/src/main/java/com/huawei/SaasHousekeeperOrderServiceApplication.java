
/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 主启动类
 *
 * @author lWX1128557
 * @since 2022-03-01
 */
@SpringBootApplication(scanBasePackages = {"com.huawei.**"})
@EnableFeignClients
@EnableEurekaClient
public class SaasHousekeeperOrderServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(SaasHousekeeperOrderServiceApplication.class, args);
    }
}
