
package com.huawei.housekeeper.service;

import com.huawei.housekeeper.controller.request.GetWorkUserInfoDto;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

/**
 * 用户服务调用
 *
 * @author lWX1128557
 * @since 2022-04-27
 */
@Component
@FeignClient(value = "saas-user-info")
public interface UserinfoService {
    /**
     * 查询工人信息
     *
     * @param workUserInfoDto 工人信息Dto
     * @return 工人信息
     */
    @PostMapping(value = "/user/info/workInfo")
    String getWorkInfo(@Valid @RequestBody GetWorkUserInfoDto workUserInfoDto);
}
