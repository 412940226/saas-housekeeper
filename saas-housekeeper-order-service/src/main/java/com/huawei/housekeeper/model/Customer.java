/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.model;

/**
 * 用户信息
 *
 * @author lWX1128557
 * @since 2022-02-23
 */
public class Customer {

    /**
     * 顾客id
     */
    private String userId;

    /**
     * 顾客姓名
     */
    private String userName;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
