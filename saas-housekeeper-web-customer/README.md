# admin

## 下载

```
npm install
```

### 本地运行

```
npm run serve
```

### 打包

```
npm run build
```

### 格式化

```
npm run lint
```
