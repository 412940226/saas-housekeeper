/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.constant;

import com.huawei.housekeeper.validator.EnumValidate;

import lombok.Getter;

import org.springframework.util.StringUtils;

/**
 * 功能描述
 *
 * @author jWX1116205
 * @since 2022-02-16
 */
@Getter
public enum StyleFlagEnum implements EnumValidate<String> {

    STYLE_FLAG_WATERFALL(1, "水瀑_waterfall"),

    STYLE_FLAG_CHARCOAL(2, "炭黑_charcoal"),

    STYLE_FLAG_FIREY_RED(3, "火红_firey_red"),

    STYLE_FLAG_ILLUMINATING(4, "明黄_ILLUMINATING");

    private int styleFlag;

    private String styleName;

    StyleFlagEnum(int styleFlag, String styleName) {
        this.styleFlag = styleFlag;
        this.styleName = styleName;
    }

    StyleFlagEnum findStyleEnumByFlag(int styleFlag) {
        for (StyleFlagEnum value : StyleFlagEnum.values()) {
            if (styleFlag == value.getStyleFlag()) {
                return value;
            }
        }
        return null;
    }

    StyleFlagEnum findStyleEnumByStyleName(String styleName) {
        for (StyleFlagEnum value : StyleFlagEnum.values()) {
            if (value.getStyleName().equals(styleName)) {
                return value;
            }
        }
        return null;
    }

    @Override
    public boolean existValidate(String value) {
        if (StringUtils.isEmpty(value)) {
            return false;
        }
        for (StyleFlagEnum testEnum : StyleFlagEnum.values()) {
            if (testEnum.getStyleFlag() == Integer.parseInt(value)) {
                return true;
            }
        }
        return false;
    }
}