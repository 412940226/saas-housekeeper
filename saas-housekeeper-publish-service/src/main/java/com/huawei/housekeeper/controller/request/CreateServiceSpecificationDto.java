/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author jWX1116205
 * @since 2022-02-10
 */
@Getter
@Setter
@ApiModel("创建服务规格对象")
public class CreateServiceSpecificationDto {

    @NotNull(message = "服务ID必填")
    @Min(value = 1L, message = "最小值：1")
    @ApiModelProperty(value = "服务ID", required = true)
    private Long serviceId;

    @ApiModelProperty("规格名称")
    private String name;

    @NotEmpty(message = "必填")
    @ApiModelProperty(value = "规格选项", required = true)
    List<String> serviceOptionList;
}