import HttpRequest from './axios';

const baseUrl = '/api-service/';
const baseLogUrl = '/api-log/';

const axios = new HttpRequest(baseUrl);
export { axios, baseLogUrl };
